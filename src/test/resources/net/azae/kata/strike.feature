# language: fr
Fonctionnalité: Strike : Je fais tomber 10 quilles en 1 lancés

  Scénario: 1
    Etant donné une partie de Bowling avec 0 lancé
    Quand je fais 4 lancés en faisant tomber respectivement : 10, 10, 10, 5 quilles
    Alors mon score total est de 75

