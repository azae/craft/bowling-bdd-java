# language: fr
# https://cucumber.io/docs/gherkin/reference/
Fonctionnalité: Exemple de fonctionalité
  Afin d'essayer le BDD sur le kata Bowling
  En tant que développeur
  Je souhaite avoir un exemple

  Scénario: Je ne fais pas tomber de quille
    Etant donné une partie de Bowling
    Quand toutes mes balles vont dans la goutière
    Alors mon score total est de 0
