# language: fr
Fonctionnalité: Spare : Je fais tomber 10 quilles en 2 lancés

  Scénario: 1
    Etant donné une partie de Bowling avec 0 lancé
    Quand je fais 4 lancés en faisant tomber respectivement : 4, 6, 4, 5 quilles
    Alors mon score total est de 23


  Scénario: 2, ce n'est pas un spare
    Etant donné une partie de Bowling avec 0 lancé
    Quand je fais 4 lancés en faisant tomber respectivement : 3, 6, 4, 5 quilles
    Alors mon score total est de 18


  Scénario: ceci n'est pas un strike
    Etant donné une partie de Bowling avec 0 lancé
    Quand je fais 4 lancés en faisant tomber respectivement : 0, 10, 4, 5 quilles
    Alors mon score total est de 23