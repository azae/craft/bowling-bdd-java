# language: fr
Fonctionnalité: Je gagne des points en faisant tomber des quilles

  Scénario: Je fais tomber entre 1 et 9 quilles en 2 lancés
   Etant donné une partie de Bowling avec 0 lancé
    Quand mon premier lancé fait tomber 1 quille
    Et mon second lancé fait tomber 0 quille
    Alors mon score total est de 1

  Scénario: Je fais tomber entre 1 et 9 quilles en 2 lancés
    Etant donné une partie de Bowling avec 0 lancé
    Quand mon premier lancé fait tomber 1 quille
    Et mon second lancé fait tomber 1 quille
    Alors mon score total est de 2

  Scénario: 4 lancés
    Etant donné une partie de Bowling avec 0 lancé
    Quand je fais 4 lancés en faisant tomber respectivement : 1, 2, 3, 4 quilles
    Alors mon score total est de 10

  Scénario: 4 lancés seconde
    Etant donné une partie de Bowling avec 0 lancé
    Quand je fais 4 lancés en faisant tomber respectivement : 5, 2, 3, 4 quilles
    Alors mon score total est de 14

