package net.azae.kata;

import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Et;
import cucumber.api.java.fr.Etantdonné;
import cucumber.api.java.fr.Quand;
import org.junit.Assert;

public class FrenshBowlingGameScenario {
    private Game game;

    @Etantdonné("^une partie de Bowling$")
    public void créerPartie() {
        game = new Game();
    }

    @Quand("^toutes mes balles vont dans la goutière$")
    public void toutesMesBallesVontDansLaGoutiere() {
        BowlingGameHelper.playAllBallsInTheGutter(game);
    }

    @Alors("^mon score total est de (\\d+)$")
    public void monScore(int score) {
        Assert.assertEquals(score, game.score());
    }

    @Etantdonné("^une partie de Bowling avec (\\d+) lancé$")
    public void unePartieDeBowlingAvecLancé(int arg0) throws Throwable {
        game = new Game();
    }

    @Quand("^mon premier lancé fait tomber (\\d+) quille$")
    public void monPremierLancéFaitTomberQuille(int arg0) throws Throwable {
        game.roll(arg0);
    }

    @Et("^mon second lancé fait tomber (\\d+) quille$")
    public void monSecondLancéFaitTomberQuille(int arg0) throws Throwable {
        game.roll(arg0);
    }

    @Et("^mon troisième lancé fait tomber (\\d+) quilles$")
    public void monTroisièmeLancéFaitTomberQuilles(int arg0) throws Throwable {
        game.roll(arg0);
    }

    @Et("^mon quatrième lancé fait tomber (\\d+) quilles$")
    public void monQuatrièmeLancéFaitTomberQuilles(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        game.roll(arg0);
    }

    @Quand("^je fais (\\d+) lancés en faisant tomber respectivement : (.*) quilles$")
    public void jeFaisLancésEnFaisantTomberRespectivement(int rollNumbers, String rolls) {
        String[] tokens = rolls.split(",");
        for (String token : tokens) {
            int pins = Integer.parseInt(token.trim());
            game.roll(pins);
        }
    }
}
